// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "ProjectEscapeCharacter.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "Runtime/Engine/Classes/Kismet/KismetSystemLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Runtime/Engine/Public/DrawDebugHelpers.h"
#include "GameFramework/CharacterMovementComponent.h"
#include <Runtime/Engine/Classes/Engine/Engine.h>

//////////////////////////////////////////////////////////////////////////
// AProjectEscapeCharacter

AProjectEscapeCharacter::AProjectEscapeCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->bAbsoluteRotation = true; // Rotation of the character should not affect rotation of boom
	CameraBoom->bDoCollisionTest = false;
	CameraBoom->TargetArmLength = 500.f;
	CameraBoom->SocketOffset = FVector(0.f, 0.f, 75.f);
	CameraBoom->RelativeRotation = FRotator(0.f, 180.f, 0.f);

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Note: The skeletal mesh and anim blueprint references on the Mesh component (inherited from Character) 
	// are set in the derived blueprint asset named MyCharacter (to avoid direct content references in C++)
}

//////////////////////////////////////////////////////////////////////////
// Input

void AProjectEscapeCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// Set up gameplay key bindings
	check(PlayerInputComponent);
	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAxis("MoveForward", this, &AProjectEscapeCharacter::MoveRight);
	PlayerInputComponent->BindAxis("MoveRight", this, &AProjectEscapeCharacter::MoveRight);

	
}


void AProjectEscapeCharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}



void AProjectEscapeCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void AProjectEscapeCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void AProjectEscapeCharacter::MoveForward(float Value)
{
	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
}

void AProjectEscapeCharacter::MoveRight(float Value)
{
	if ( (Controller != NULL) && (Value != 0.0f) )
	{
		// find out which way is right
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);
	
		// get right vector 
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		// add movement in that direction
		AddMovementInput(Direction, Value);
	}
}

void AProjectEscapeCharacter::insertObject(FString a)
{
	inventoryObject_.Add(a);
}

bool AProjectEscapeCharacter::searchObject(FString find)
{
	int32 value = inventoryObject_.Find(find);

	//GEngine->AddOnScreenDebugMessage(0, 2.f, FColor::Yellow, find + " " + FString::FromInt(value));
	if (value >= 0) {
		return true;
	}
	else {
		return false;
	}
}


void AProjectEscapeCharacter::addCount(int value) {
	score_ += value;
}

int AProjectEscapeCharacter::getScore() {
	return score_;
}

bool AProjectEscapeCharacter::Hook(FVector start, FVector end) {
	FHitResult result;
	FCollisionQueryParams *traceparams = new FCollisionQueryParams();
	DrawDebugLine(GetWorld(), start, end, FColor(255, 0, 0), true, 0.1f);
	if (GetWorld()->LineTraceSingleByChannel(result, start, end, ECollisionChannel::ECC_Visibility, *traceparams)) {
		savePositiontoMove = result.Location;
		if (result.GetActor() != nullptr) {
			//GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("Colliding"));
			if (Hooked == true) {
				//GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, FString::Printf(TEXT("X %d Y %d Z %d"), result.Location.X, result.Location.Y, result.Location.Z));
			}
			return true;
		}
	}
	return false;
}

void AProjectEscapeCharacter::movegraped() {
	;
	FVector tmp = savePositiontoMove - GetActorLocation();
	tmp = tmp * (GetWorld()->GetDeltaSeconds() * 250);
	LaunchCharacter(tmp, true, true);
}

void AProjectEscapeCharacter::StopHooked() {
	Hooked = false;
	HookMoveFinished = false;

}

