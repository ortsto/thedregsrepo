// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "ProjectEscapeCharacter.generated.h"

UCLASS(config=Game)
class AProjectEscapeCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;
public:
	AProjectEscapeCharacter();

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;

	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;

protected:

	/** Resets HMD orientation in VR. */
	void OnResetVR();

	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/** 
	 * Called via input to turn at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);


protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

public:

	UFUNCTION(BlueprintCallable, Category = "Inventory")
		void insertObject(FString a);
	UFUNCTION(BlueprintCallable, Category = "Inventory")
		bool searchObject(FString find);
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = KeysGame)
		TArray<FString> inventoryObject_;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = KeysGame)
		FVector mousepositionB;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Camera)
		FVector mousedirectionB;
	int score_;
	UFUNCTION(BlueprintCallable, Category = "Score")
		void addCount(int value);
	UFUNCTION(BlueprintCallable, Category = "Score")
		int getScore();
	UFUNCTION(BlueprintCallable, Category = "Hook")
		bool Hook(FVector start, FVector end);
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Camera)
		FVector savePositiontoMove;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Camera)
		bool Hooked;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Camera)
		bool HookMoveFinished;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Camera)
		bool Swinged;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Pickup)
		bool isPicking_;
	UFUNCTION(BlueprintCallable, Category = "Hook")
		void movegraped();
	UFUNCTION(BlueprintCallable, Category = "Hook")
		void StopHooked();
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Pickup)
		FString playername_;



	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }
};

