// Fill out your copyright notice in the Description page of Project Settings.

#include "Coin.h"
#include "ProjectEscapeCharacter.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ACoin::ACoin()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	RootComponent = mesh;
	box = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollision"));
	box->SetupAttachment(RootComponent);
	box->OnComponentBeginOverlap.AddDynamic(this, &ACoin::OnOverlapBegin);
}

// Called when the game starts or when spawned
void ACoin::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACoin::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ACoin::OnOverlapBegin_Implementation(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor && (OtherActor != this) && OtherComp) {
		AProjectEscapeCharacter* player = Cast<AProjectEscapeCharacter>(UGameplayStatics::GetPlayerCharacter(GetWorld(), 0));
		if (player == OtherActor) {
			player->addCount(5);
			player->isPicking_ = true;
			this->Destroy();
		}
	}

}
